#          ___                                 __
#   ____ _/ (_)___ _________  _________  _____/ /_
#  / __ `/ / / __ `/ ___/ _ \/ ___/_  / / ___/ __ \
# / /_/ / / / /_/ (__  )  __(__  ) / /_(__  ) / / /
# \__,_/_/_/\__,_/____/\___/____(_)___/____/_/ /_/

alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias -- -="cd -"

alias g="git"
alias gs="git status"
alias h="history"
alias j="jobs"

# Kill unattached tmux sessions
alias tkill="tmux ls | grep -v attached | awk '{ print $1 }' | sed 's/://' | cut -d ' ' -f1 | xargs -I {} tmux kill-session -t {}"

# Stopwatch
alias timer='echo "Timer started. Stop with Ctrl-D." && date && time cat && date'

# IP addresses
alias myip="dig +short myip.opendns.com @resolver1.opendns.com"
alias localip="ipconfig getifaddr en0"
alias ips="ifconfig -a | grep -o 'inet6\? \(addr:\)\?\s\?\(\(\([0-9]\+\.\)\{3\}[0-9]\+\)\|[a-fA-F0-9:]\+\)' | awk '{ sub(/inet6? (addr:)? ?/, \"\"); print }'"

# Flush Directory Service cache
alias flush="dscacheutil -flushcache && killall -HUP mDNSResponder"

# View HTTP traffic
alias sniff="sudo ngrep -d 'en1' -t '^(GET|POST) ' 'tcp and port 80'"
alias httpdump="sudo tcpdump -i en1 -n -s 0 -w - | grep -a -o -E \"Host\: .*|GET \/.*\""

alias https="http --default-scheme=https"

# URL-encode strings
alias urlencode='python3 -c "import sys, urllib.parse as ul; print(ul.quote_plus(sys.argv[1]))"'

# Reload the shell (i.e. invoke as a login shell)
alias reload="exec $SHELL -l"

alias vim="nvim"
alias vi="nvim"
alias e="launch-emacs"

# resize tmux windows
alias fit="tmux detach -a"

# Verbose tar.gz compression and expansion
alias tarc="tar -cvzf"
alias tarx="tar -xvzf"

# tmux
alias ta='tmux attach -t'
alias tad='tmux attach -d -t'
alias ts='tmux new-session -s'
alias tl='tmux list-sessions'
alias tksv='tmux kill-server'
alias tkss='tmux kill-session -t'

# lsd
alias le="lsd"
alias la="lsd -lah"

# LazyDocker
alias ld="lazydocker"

# Create a new directory and enter it
# Create a relative path to arg1 from ar2
relpath() {
  python -c "import os.path; print os.path.relpath('$1','${2:-$PWD}')";
}

# Determine size of a file or total size of a directory
fs() {
  if du -b /dev/null > /dev/null 2>&1; then
    local arg=-sbh;
  else
    local arg=-sh;
  fi
  if [[ -n "$@" ]]; then
    du $arg -- "$@";
  else
    du $arg .[^.]* *;
  fi;
}

# Compare original and gzipped file size
gz() {
  local origsize=$(wc -c < "$1");
  local gzipsize=$(gzip -c "$1" | wc -c);
  local ratio=$(echo "$gzipsize * 100 / $origsize" | bc -l);
  printf "orig: %d bytes\n" "$origsize";
  printf "gzip: %d bytes (%2.2f%%)\n" "$gzipsize" "$ratio";
}

# Syntax-highlight JSON strings or files
# Usage: `json '{"foo":42}'` or `echo '{"foo":42}' | json`
json() {
  if [ -t 0 ]; then
    python -mjson.tool <<< "$*" | pygmentize -l javascript;
  else # pipe
    python -mjson.tool | pygmentize -l javascript;
  fi;
}

# Run `dig` and display the most useful info
digga() {
  dig +nocmd "$1" any +multiline +noall +answer;
}

# UTF-8-encode a string of Unicode symbols
escape() {
  printf "\\\x%s" $(printf "$@" | xxd -p -c1 -u);
  # print a newline unless we’re piping the output to another program
  if [ -t 1 ]; then
    echo ""; # newline
  fi;
}

# Decode \x{ABCD}-style Unicode escape sequences
unidecode() {
  perl -e "binmode(STDOUT, ':utf8'); print \"$@\"";
  # print a newline unless we’re piping the output to another program
  if [ -t 1 ]; then
    echo ""; # newline
  fi;
}

# Get a character’s Unicode code point
codepoint() {
  perl -e "use utf8; print sprintf('U+%04X', ord(\"$@\"))";
  # print a newline unless we’re piping the output to another program
  if [ -t 1 ]; then
    echo ""; # newline
  fi;
}

# show terminal colors
termcol () {
  for code ({000..255}) print -P -- "$code: %F{$code}Isn't this a fun color?%f"
}

# benchmark of zsh startup
timeshell() {
  for i in $(seq 1 10); do /usr/bin/time zsh -i -c exit; done
}

envf() {
  local envfile
  envfile="$1"

  if [ -z "$envfile" ]; then
    envfile="build/test-environment"
  fi
  gsed 's/^export\s//i' "$envfile" >> .env
}

untagged() {
  git log $(git describe --tags --abbrev=0)..HEAD --oneline
}

redraw-prompt() {
  local precmd
  for precmd in $precmd_functions; do
    $precmd
  done
  zle reset-prompt
}
zle -N redraw-prompt

# browse files in a new pane
file-manager() {
  tmux new-window -c "#{pane_current_path}" "n"
}
zle -N file-manager
bindkey '^O' file-manager

# fuzzy checkout a git branch
fuzzy-branches() {
  . ~/.scripts/fuzzy-branches
  zle redraw-prompt
}
zle -N fuzzy-branches
bindkey '^b' fuzzy-branches

# FIXME: not compatible with zsh-vi-mode
# recent-dirs() {
#   . ~/.scripts/recent-dirs
#   # zle redraw-prompt
# }
# zle -N recent-dirs
# bindkey '^J' recent-dirs

# Edit a frecent file
recent-files() {
  ~/.scripts/recent-files
  zle redraw-prompt
}
zle -N recent-files
bindkey '^h' recent-files

findfile() {
  ~/.scripts/find-file
  zle redraw-prompt
}
zle -N findfile
bindkey '^f' findfile

# fzf based process killer
fuzzy-kill() {
  ~/.scripts/fuzzy-kill
  zle redraw-prompt
}
zle -N fuzzy-kill
bindkey '^p' fuzzy-kill

fuzzy-grep() {
  ~/.scripts/fuzzy-grep
  zle redraw-prompt
}
zle -N fuzzy-grep
bindkey '^G' fuzzy-grep

fuzzy-font() {
  theme font "$(theme list fonts | fzf)"
  zle redraw-prompt
}
zle -N fuzzy-font
bindkey '^Y' fuzzy-font

fuzzy-theme() {
  theme colors "$(theme list colors | fzf)"
  zle redraw-prompt
}
zle -N fuzzy-theme
bindkey '^N' fuzzy-theme

# nnn cd on quit
n() {
    # Block nesting of nnn in subshells
    if [ -n $NNNLVL ] && [ "${NNNLVL:-0}" -ge 1 ]; then
        echo "nnn is already running"
        return
    fi

    # The behaviour is set to cd on quit (nnn checks if NNN_TMPFILE is set)
    # To cd on quit only on ^G, either remove the "export" as in:
    #    NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"
    #    (or, to a custom path: NNN_TMPFILE=/tmp/.lastd)
    # or, export NNN_TMPFILE after nnn invocation
    export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

    # Unmask ^Q (, ^V etc.) (if required, see `stty -a`) to Quit nnn
    # stty start undef
    # stty stop undef
    # stty lwrap undef
    # stty lnext undef

    nnn -edH "$@"

    if [ -f "$NNN_TMPFILE" ]; then
            . "$NNN_TMPFILE"
            rm -f "$NNN_TMPFILE" > /dev/null
    fi
}

# set up some macOS specific aliases
if [[ $OSTYPE == 'darwin'* ]]; then
  source ~/.zsh/macos.zsh
fi

# set up linux aliases
if [[ $OSTYPE == 'linux-gnu' ]]; then
  source ~/.zsh/linux.zsh
fi
